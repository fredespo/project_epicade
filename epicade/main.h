#ifndef _MAIN_H
#define _MAIN_H
#include <joystick.h>
#include <nes.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "neslib.h"

typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;

typedef int32_t s32;
typedef int16_t s16;
typedef int8_t s8;

extern u8 i,j;

extern void bankswitch(const u8 bank, const u8 file);


struct sprite
{
	u16 x; 
	u8 y;
    u8 tileNum;
    u8 attr;
    u8 dirH, dirV;
    u8 state;
};

enum 
{
    DIR_DOWN=0,
    DIR_UP,
    DIR_LEFT,
    DIR_RIGHT,
};

extern u8 ctrl, prevctrl, oam_off;
#pragma zpsym("ctrl")
#pragma zpsym("prevctrl")
#pragma zpsym("oam_off")

#endif // _MAIN_H
