#include "main.h"
#include "sprites.h"
#include "player.h"

extern u8 ctrl;
extern u16 scr;
extern u16 level_len;

u8 tileBelow, tileOn, tileWayBelow, tileRight, tileLR, tileUR;
u16 pIndex;

struct sprite *player;

void initPlayer(struct sprite *p)
{
	p->x = 24;
	p->y = 70;
	p->tileNum = 0x00;
	p->attr = 1;
	p->dirH=0;
	p->dirV=0;
	p->state = 0;
	player = p;
}


void playerUp()
{
	if(tileRight==0x69)
	{
		player->y--;
	}

	if(tileLR==0x69) playerRight();
}

void playerRight()
{
	if(player->x < 24)
	{
		player->x++;
	}
	else if (scr < level_len) 
	{
		scr++;
	}
	else if(player->x < 248)
	{
		player->x++;
	}

	if(tileRight == 0x69 && tileBelow==0x69) player->y--;
}

void playerLeft()
{
	if (player->x<=24 && scr) 
	{
		scr--;
	}
	else if(player->x)
	{
		player->x--;
	}
}

void playerStorePos()
{
	pIndex = getBgIndex(player->x, player->y);
	tileWayBelow = getBgTile(pIndex+2);
	tileRight = getBgTile(pIndex+22);
	tileLR = getBgTile(pIndex+23);
	tileUR = getBgTile(pIndex+21);

	loadSpriteTileBG(player);
	tileBelow = getBgTile(pIndex+1);
	tileOn = getBgTile(pIndex);
}

void playerUpdate()
{
	if(isPlayerOnGround() || isPlayerOnStairs())
	{
		//snap position
		player->y = (player->y >> 3) << 3;
	}
	else
	{
		//apply gravity
		player->y+=4;
		player->dirV = DIR_DOWN;
	}

	//oam_off = oam_spr((p->x >> 3) << 3, (p->y >> 3) << 3, 0x7e, 2, oam_off);
}

u8 isTileFloor(u8 tile)
{
	if(tile==0x10 || tile==0x15 || tile==0x16) return 1;
	return 0;
}

u8 isTileStair(u8 tile)
{
	if(tile==0x69) return 1;
	return 0;
}

u8 isPlayerOnStairs()
{
	if(isTileStair(tileLR) || isTileStair(tileBelow) || isTileStair(tileRight)) return 1;
	return 0;
}

u8 isPlayerOnGround()
{
	if(isTileFloor(tileBelow) || isTileFloor(tileLR)) return 1;
	return 0;
}

