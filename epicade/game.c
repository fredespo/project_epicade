#include "main.h"
#include "engine.h"
#include "sprites.h"

#include "level.h"
#include "HUD_nam.h"

// To avoid banking overlaps the palettes are just included here (there are not many)
/*
#include "level1a.pal.h"
#include "level1b.pal.h"
#include "level2a.pal.h"
#include "level3.pal.h"
#include "level4.pal.h"
#include "level5.pal.h"
#include "level6.pal.h"
*/

#include "levels.pal.h"

#include "sprites.h"
#include "player.h"

struct sprite player;

u16 scr;
u8 i;


u16 level_pos_offs_r;
u16 level_pos_offs_l;
u8 nam_offs_l;
u8 nam_offs_r;

const u8 nam_pos_r[] = {
                     33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,
                     0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,
                     };

const u8 nam_pos_l[] = {
                     63,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,
                     33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,
                     };


const u8 *level_dats[] = { 
					level1a,
					level1b,
					level1c,
					level1d,

                    level2a,
                    level2b,
                    level2c,

                    level3a,
                    level3b,
                    level3c,
                    level3d,

                    level4a,
                    level4b,
                    level4c,
                    level4d,

                    level5a,
                    level5b,
                    level5c,
                    level5d,

                    level6a,
                    level6b,

                    levelfinal, 
					};

const u8 *level_attrs[] = {
					level1a_attr,
					level1b_attr,
					level1c_attr,
					level1d_attr,

					level2a_attr,
					level2b_attr,
					level2c_attr,

					level3a_attr,
					level3b_attr,
					level3c_attr,
					level3d_attr,

                    level4a_attr,
                    level4b_attr,
                    level4c_attr,
                    level4d_attr,

                    level5a_attr,
                    level5b_attr,
                    level5c_attr,
                    level5d_attr,

                    level6a_attr,
                    level6b_attr,

                    levelfinal_attr, 
					};

const u8 level_banks[] =  { 0,0,1,1, 1,2,2, 3,3,3, 4,4, 5,5,5, 6,6,6, 7,7, 8,8 };
					
const u8 level_chrs[] =   { 0,0,0,4, 8,8,8, 12,12,12,12, 16,16,16,16, 20,20,20,20, 24,24,24,24, 24 }; 

const u8 *level_pals[] = {
					level1a_pal,
					level1a_pal,
					level1a_pal,
					level1b_pal,

					level2a_pal,
					level2a_pal,
					level2a_pal,

					level3_pal,
					level3_pal,
					level3_pal,
					level3_pal,
				
					level4_pal,
					level4_pal,
					level4_pal,
					level4_pal,
			
					level5_pal,
					level5_pal,
					level5_pal,
					level5_pal,

					level6_pal,
					level6_pal,

					level6_pal
				   };

const u16 level_lens[] = {
				512,	
				1280,
				512,
				(63 * 2 * 8) - 256,

				512,
				512,
				512,

				512,	
				512,	
				512,	
				512,	


				512,	
				512,	
				512,	
				512,	


				512,	
				512,	
				512,	
				512,	


				512,	
				512,	
				512,	
				512,	


				512,	
				512	
				  };



void gameinit() {
		u8 i;
		scr = 0;

        initPlayer(&player);

        ppu_off();
        vram_adr(0x23c0);
        vram_fill(0x00, 0x3f);
        vram_put(128);

		vram_adr(NAMETABLE_A);
		vram_fill(0xff, 0x3bf);
        vram_adr(NAMETABLE_B);
        vram_fill(0xff, 0x3bf);

		vram_adr(NAMETABLE_A);
		vram_unrle(HUD_nam);
        vram_adr(0x23c0);
        vram_fill(0xff, 0x0f);



		vram_adr(0x1000);
		vram_fill(0xff, 0x08);



        level_init(1);

        level_pos_offs = 0;
        nam_offs = 0;

        ppu_on_all();

        for (i=0; i<63; i++) {
            level_pos_offs = i;         // this should increase to the end of the level
            nam_offs = i;               // this is oscillating between 0-31 and 32-63
            level_render_col();
            ppu_wait_nmi();
        }


        ppu_on_all();
}


void gameloop() {
	u8 i;

    while (1) {
        u8 ctrl, prevctrl;


		scroll(0,0);
		ppu_wait_nmi();
		
        clearSprites();
        drawSpriteZero();
        drawSprite(&player);
        playerStorePos();

        ctrl = joy_read(0);

        if ((ctrl & PAD_RIGHT)) {
           playerRight();

            if ((scr & 0x07) == 0x07) {         //  new column at every 8 pixels

                i = (scr >> 0x03) & 0x3f;
                nam_offs_r = nam_pos_r[i];
                level_pos_offs_r = (scr >> 0x03) + 33;

                // copy external variables into scroll engine parameters
                level_pos_offs = level_pos_offs_r;
                nam_offs = nam_offs_r;
                level_render_col();
            }
        }

        if ((ctrl & PAD_LEFT)) {
            playerLeft();

            if ((scr & 7) == 0)  {         //  new column at every 8 pixels

                i = (scr >> 0x03) & 0x3f;
                nam_offs_l = nam_pos_l[i];
                level_pos_offs_l = (scr >> 0x03) -1;


                level_pos_offs = level_pos_offs_l;
                nam_offs = nam_offs_l;

                level_render_col();
            }
        }


        if(ctrl&PAD_UP) playerUp();
        playerUpdate();

		split(scr,0);




       // ppu_wait_nmi();
        prevctrl = ctrl;
    }
}
