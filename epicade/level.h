#ifndef _LEVEL_1_H
#define _LEVEL_1_H

extern const unsigned char level1a[];
extern const unsigned char level1b[];
extern const unsigned char level1c[];
extern const unsigned char level1d[];

extern const unsigned char level2a[];
extern const unsigned char level2b[];
extern const unsigned char level2c[];

extern const unsigned char level3a[];
extern const unsigned char level3b[];
extern const unsigned char level3c[];
extern const unsigned char level3d[];

extern const unsigned char level4a[];
extern const unsigned char level4b[];
extern const unsigned char level4c[];
extern const unsigned char level4d[];

extern const unsigned char level5a[];
extern const unsigned char level5b[];
extern const unsigned char level5c[];
extern const unsigned char level5d[];

extern const unsigned char level6a[];
extern const unsigned char level6b[];

extern const unsigned char levelfinal[];

extern const u8 level1a_pal[];
extern const u8 level1a_pal[];
extern const u8 level1a_pal[];
extern const u8 level1b_pal[];
extern const u8 level2a_pal[];
extern const u8 level3_pal[];
extern const u8 level4_pal[];
extern const u8 level5_pal[];
extern const u8 level6_pal[];

extern const unsigned char level1a_attr[];
extern const unsigned char level1b_attr[];
extern const unsigned char level1c_attr[];
extern const unsigned char level1d_attr[];

extern const unsigned char level2a_attr[];
extern const unsigned char level2b_attr[];
extern const unsigned char level2c_attr[];

extern const unsigned char level3a_attr[];
extern const unsigned char level3b_attr[];
extern const unsigned char level3c_attr[];
extern const unsigned char level3d_attr[];

extern const unsigned char level4a_attr[];
extern const unsigned char level4b_attr[];
extern const unsigned char level4c_attr[];
extern const unsigned char level4d_attr[];

extern const unsigned char level5a_attr[];
extern const unsigned char level5b_attr[];
extern const unsigned char level5c_attr[];
extern const unsigned char level5d_attr[];

extern const unsigned char level6a_attr[];
extern const unsigned char level6b_attr[];

extern const unsigned char levelfinal_attr[];

#endif // _LEVEL_1_H
