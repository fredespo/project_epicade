#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>


FILE *lout;
FILE *aout;

char lnam[64];
char anam[64];


#define LEVEL_HEIGHT 11

FILE *level;
FILE *tiles;

unsigned char line[16][16384];

unsigned char leveldata[16][16384];

unsigned char nam[1024];

unsigned char fnam1[1024];		// flattened nam even
unsigned char fnam2[1024];		// flattened nam odd

unsigned char attr[64];			

unsigned char attr1[1024]; 		// flattened attr!

unsigned char attr_a;


unsigned char attr_data[1024][16];


char out1[2048];	// output hexdata. a full column.
char out2[2048];	// output hexdata. a full column.

char attrout[512]; // attr output are much shorter


char frag[32];

char *sep = ",";
char *tile_id_s;
int tile_id;

unsigned char tile_tl, tile_tr, tile_bl, tile_br;


int x,y,i,j,r;
int xx,yy;

const unsigned char tileid2attrx[] = {
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
					0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,
};

const unsigned char tileid2attry[] = {  
					0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
					8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
					8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
					16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,
					16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,
					24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,
					24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,
					32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,	
					32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,	
					40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,
					40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,
					48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,
					48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,
					56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,
					56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,56,
};
					
const unsigned char tileid2attrm[] = {
					0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,
			
                                        0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,

                                       0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,

                                        0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,

                                        0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,

                                        0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,

                                        0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,

                                        0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,

                                        0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,

                                        0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,

                                       0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,

                                        0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,

                                        0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,

                                        0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,

                                        0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x0c,

                                        0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0,0x30,0xc0

};




					
					



unsigned char getattr(tile_id) {
int attrx, attry;
unsigned char attrb, attrm;


	attrx = tileid2attrx[tile_id];
	attry = tileid2attry[tile_id];
	attrm = tileid2attrm[tile_id];

	attrb = attr[attry + attrx] & attrm;
	 
	if (attrm == 0xc0) return (attrb >> 6) & 0x03;
	if (attrm == 0x30) return (attrb >> 4) & 0x03;
	if (attrm == 0x0c) return (attrb >> 2) & 0x03;
	if (attrm == 0x03) return (attrb >> 0) & 0x03;


}


int main(int argc, char *argv[]) {

 	level = fopen(argv[1], "r");
 	tiles = fopen(argv[2], "r");

// slurp all tile lines into arrays

sprintf(lnam,"%s_dat.h", argv[3]);
lout = fopen(lnam,"w");
sprintf(anam,"%s_attr.h", argv[3]);
aout = fopen(anam,"w");



fprintf(lout,"const unsigned char %s[] = {\n", argv[3]);
fprintf(aout,"const unsigned char %s_attr[] = {\n", argv[3]);

// slurp NAM into arrays

	for (i=0; i< LEVEL_HEIGHT; i++) {
		fgets(line[i], 16384, level);
	}

	fread(nam, 1, 1024, tiles);

// slurp attributes

    for (i=0; i<64; i++) 
        attr[i] = nam[0x3c0 + i];






/*
for (i = 0; i < 64; i++) {
    printf("%02x ",attr[i]);
}
printf("\n\n");
*/




// flatten NAM so we can address it easily
// sequentially accessed but: two arrays one for even and one for odd lines.

	
	y = 0;
	x = 0;
	for (i = 0; i < 480 ; i++) {
		fnam1[i] = nam[y];
		fnam2[i] = nam[y + 0x20];
		if (x == 0x1f) {
			y += 1 + 0x20;
			x = 0;
		}
		else {
			y++;	
			x++;
		}
	}
	

// conversion begins
// The level height is tweakable right here in the loop: 

	for (y = 0; y < LEVEL_HEIGHT ; y++) {
		x = 0;

		for (tile_id_s = strtok(line[y], sep); tile_id_s; tile_id_s = strtok(NULL, sep)) {
			tile_id = atoi(tile_id_s);		
			leveldata[y][x] = tile_id;
			x++;
		}	
		xx = x;
	}	

	j = 0;

	for (x = 0; x < xx; x++) {	
			memset(out1, 0, sizeof(out1));
			memset(out2, 0, sizeof(out2));
			memset(attrout, 0, sizeof(attrout));

			j = x & 0x03; 	// j is the placement of attr bits for a tile
			
			if (j == 0) attr_a = 0;

		for (y = 0; y < LEVEL_HEIGHT ; y++) {

			tile_id = leveldata[y][x];

			tile_tl = fnam1[tile_id*2];
			tile_tr = fnam1[(tile_id*2)+1];
			tile_bl = fnam2[tile_id*2];
			tile_br = fnam2[(tile_id*2)+1];

			// we got the tile, now pick the attr
			// we run through the attr_data multiple (4) times

			if (((x % 2) == 0) && (y % 2) == 0) r = 0;
			if (((x % 2) == 1) && (y % 2) == 0) r = 2;
			if (((x % 2) == 0) && (y % 2) == 1) r = 4;
			if (((x % 2) == 1) && (y % 2) == 1) r = 6;

			attr_data[x/2][y/2] |= getattr(tile_id) << r;
			
			sprintf(frag,"0x%02x,0x%02x,",tile_tl, tile_bl);
			strcat(out1, frag);
			sprintf(frag,"0x%02x,0x%02x,",tile_tr, tile_br);
			strcat(out2, frag);

		}
		
		fprintf(lout,"%s \n", out1);
		fprintf(lout,"%s \n", out2);


	}




	fclose(level);
	fclose(tiles);


fprintf(lout,"};\n\n");


//fprintf("const unsigned char %s_attr[] = {\n", argv[3]);

for (x = 0; x < (xx/2) ; x++) {
	for (y = 0; y < ((LEVEL_HEIGHT+1) / 2) ; y++) {
		fprintf(aout,"0x%02x,", attr_data[x][y]);
	}
	fprintf(aout,"0x00, 0x00,"); 	// <<<< -- pad to 8 bytes if scroll is smaller than a whole screen
	fprintf(aout,"\n");
}
fprintf(aout,"};\n\n");


}




