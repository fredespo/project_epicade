#ifndef _PLAYER_H
#define _PLAYER_H

extern void initPlayer(struct sprite *p);
extern void playerUpdate(void);
extern void playerUp(void);
extern void playerStorePos(void);
extern void playerRight(void);
extern void playerLeft(void);
extern u8 isTileFloor(u8 tile);
extern u8 isTileStair(u8 tile);
extern u8 isPlayerOnStairs(void);
extern u8 isPlayerOnGround(void);

#endif
