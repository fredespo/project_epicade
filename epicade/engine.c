#include "main.h"
#include "engine.h"
#include "game.h"
#include "level.h"

//#pragma rodata-name (push,"PRG2")

// --------------------------------------------------------------------------
// some tweakable variables

// Level height in CHR-s
#define LEVEL_HEIGHT 22

// Level offset from the top in CHR-s
#define LEVEL_OFFS 8






u16 level_pos_offs;		// offset into level data
u8 nam_offs;			// offset into tilemap
u8 vrambuf[128];
u16 addr;
u8 level_id;

u8 *level_pal;
u8 *level_dat;
u8 *level_attr;
u16 level_len;


//------------------------------
// Internal to the tile renderer




const u8 attr_mul[] = {
	0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,
	0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7
};



//#pragma rodata-name (pop)
//#pragma code-name (push,"PRG2")

void level_init(u8 num) {
	pal_bg((u8 *)level_pals[num]);
	pal_spr((u8 *)level_pals[num]);
	level_dat = (u8 *)level_dats[num];
    level_attr = (u8 *)level_attrs[num];
	level_len = level_lens[num];

	bankswitch(0, level_chrs[num]);
	bankswitch(1, level_chrs[num]+2);

	bankswitch(6, level_banks[num]);
}



// parameters: 
// - level_pos_offs -> offset into the level data
// - nam_offs		-> offset into VRAM columns


void level_render_col(void) {
		u8 i,j;
		u16 aoffs;
		u16 offs;
		u16 tileaddr;
		u16 attroffs;		
		u16 attraddr;		

	
		attroffs = attr_mul[nam_offs];

		if (nam_offs > 31) {
			tileaddr = NTADR_B(0,LEVEL_OFFS) + (nam_offs - 32); 
			attraddr = 0x27d0     + attroffs;					// <<<<<<<<<<<< remember to adjust it with the offset !
		} 
		else {
			tileaddr = NTADR_A(0,LEVEL_OFFS)  + nam_offs; 	    // tilemap offset can't be more than $7f anyway
			attraddr = 0x23d0      + attroffs;					// <<<<<<<<<<<<< this too!
		}
		

		//offs = mul30[level_pos_offs];			// NAM columns contain 30 CHRs - (offs is a multiple of 30) for full screen height

		
		offs = LEVEL_HEIGHT * level_pos_offs;			// didn't want to do this but the multiplication table takes up too much space

		aoffs = (level_pos_offs >> 2) << 3;		// ATTR columns are 8 bytes so aoffs is a multiple of 8 - (also attr offset is level_pos_offs / 4)
												// INFO: it is always padded to 8 bytes to leave this bitshift in place
												




		vrambuf[0] = MSB(tileaddr) | NT_UPD_VERT;

		vrambuf[1] = LSB(tileaddr); 
		vrambuf[2] = LEVEL_HEIGHT;

		j = 3;
	
		for (i=0 ; i < LEVEL_HEIGHT; i++) {
			vrambuf[3+i] = level_dat[offs+i];			// <<<<<< level data array should be a pointer
			j++;
		}

		i = 0;


		while (i < 6) {								// <<<<<<<<<< should be adjusted to level height (8 is full screen)
			vrambuf[j] = MSB(attraddr);
			j++;
			vrambuf[j] = LSB(attraddr);
			j++;
			vrambuf[j] = level_attr[aoffs];
			j++;
			attraddr += 8;						
			aoffs++;
			i++;
		}

		vrambuf[j] = NT_UPD_EOF;
		set_vram_update(vrambuf);
	
/*
			vram_adr(attraddr);
			vram_put(level1_attr[aoffs]);
			attraddr += 8;
			aoffs++;
			i++;
*/
		
}




//#pragma code-name (pop)
