const u8 metaspr_player [2][19][49] = 
{
    //direction:left
    {
        //walking frame 1/4
        {
            0,0,0x00,0,     
            8,0,0x01,0,

            0,8,0x10,0,
            8,8,0x11,0,

            0,16,0x20,1,
            8,16,0x21,1,

            0,24,0x30,1,
            8,24,0x31,1,

            128
        },

        //walking frame 2/4
        {
            0,0,0x02,0,     
            8,0,0x03,0,

            0,8,0x12,0,
            8,8,0x13,0,

            0,16,0x22,1,
            8,16,0x23,1,

            0,24,0x32,1,
            8,24,0x33,1,

            128
        },

        //walking frame 3/4
        {
            0,0,0x04,0,     
            8,0,0x05,0,

            0,8,0x14,0,
            8,8,0x15,0,

            0,16,0x24,1,
            8,16,0x25,1,

            0,24,0x34,1,
            8,24,0x35,1,

            128
        },

        //walking frame 4/4 - same as walking 2/4



        //crouching frame 1/1
        {
            0,8,0x06,0,
            8,8,0x07,0,

            0,16,0x16,0,
            8,16,0x17,0,

            0,24,0x26,1,
            8,24,0x27,1,

            128
        },


        //walking up stairs frame 1/2
        {
            0,0,0x08,0,
            8,0,0x09,0,
            
            0,8,0x18,0,
            8,8,0x19,0,

            0,16,0x28,1,
            8,16,0x29,1,

            0,24,0x38,1,
            8,24,0x39,1,
            16,24,0x3a,1,

            128
        },

        //walking up stairs frame 2/2 - same as walking frame 2/4



        //walking down stairs frame 1/2
        {
            0,0,0x0b,0,
            8,0,0x0c,0,

            0,8,0x1b,0,
            8,8,0x1c,0,

            0,16,0x2b,1,
            8,16,0x2c,1,

            0,24,0x3b,1,

            128
        },

        //walking down stairs frame 2/2 - same as walking frame 2/4


        //dead frame 1/1
        {
            0,0,0x0d,0,

            0,8,0x1d,0,
            8,8,0x1e,0,
            16,8,0x2d,1,
            24,8,0x2e,1,
            32,8,0x2f,1,

            128
        },

        //whipping standing frame 1/3
        {
            0,0,0x40,0,
            8,0,0x41,0,
            16,0,0x42,0,

            0,8,0x50,0,
            8,8,0x51,0,
            16,8,0x52,0,

            0,16,0x60,1,
            8,16,0x61,1,
            16,16,0x62,1,

            0,24,0x70,1,
            8,24,0x71,1,

            128
        },

        //whipping standing frame 2/3
        {
            0,0,0x43,0,     
            8,0,0x44,0,

            0,8,0x53,0,
            8,8,0x54,0,

            0,16,0x63,1,
            8,16,0x64,1,

            0,24,0x73,1,
            8,24,0x74,1,

            128
        },


        //whipping standing frame 3/3
        {
            8,0,0x46,0,
            16,0,0x47,0,

            0,8,0x55,0,
            8,8,0x56,0,
            16,8,0x57,0,

            0,16,0x65,1,
            8,16,0x66,1,
            16,16,0x67,1,

            0,24,0x75,1,
            8,24,0x76,1,
            16,24,0x77,1,

            128
        },


        //whipping crouching frame 1/3
        {
            0,0,0x40,0,
            8,0,0x41,0,
            16,0,0x42,0,

            0,8,0x50,0,
            8,8,0x51,0,
            16,8,0x52,0,

            0,16,0x16,1,
            8,16,0x17,1,

            0,24,0x26,1,
            8,24,0x27,1,

            128
        },

         //whipping crouching frame 2/3
        {
            0,0,0x43,0,     
            8,0,0x44,0,

            0,8,0x53,0,
            8,8,0x54,0,

            0,16,0x16,1,
            8,16,0x17,1,

            0,24,0x26,1,
            8,24,0x27,1,

            128
        },

         //whipping crouching frame 3/3
        {
            8,0,0x46,0,
            16,0,0x47,0,

            0,8,0x55,0,
            8,8,0x56,0,
            16,8,0x57,0,

            0,16,0x16,1,
            8,16,0x17,1,

            0,24,0x26,1,
            8,24,0x27,1,

            128
        },



        //whipping climbing up stairs frame 1/3
        {
            0,0,0x40,0,
            8,0,0x41,0,
            16,0,0x42,0,

            0,8,0x50,0,
            8,8,0x51,0,
            16,8,0x52,0,

            0,16,0x28,1,
            8,16,0x29,1,

            0,24,0x38,1,
            8,24,0x39,1,
            16,24,0x3a,1,

            128
        },

         //whipping climbing up stairs frame 2/3
        {
            0,0,0x43,0,     
            8,0,0x44,0,

            0,8,0x53,0,
            8,8,0x54,0,

            0,16,0x28,1,
            8,16,0x29,1,

            0,24,0x38,1,
            8,24,0x39,1,
            16,24,0x3a,1,

            128
        },

         //whipping climbing up stairs frame 3/3
        {
            8,0,0x46,0,
            16,0,0x47,0,

            0,8,0x55,0,
            8,8,0x56,0,
            16,8,0x57,0,

            0,16,0x28,1,
            8,16,0x29,1,

            0,24,0x38,1,
            8,24,0x39,1,
            16,24,0x3a,1,

            128
        },


        //whipping climbing down stairs frame 1/3
        {
            0,0,0x40,0,
            8,0,0x41,0,
            16,0,0x42,0,

            0,8,0x50,0,
            8,8,0x51,0,
            16,8,0x52,0,

            0,16,0x2b,1,
            8,16,0x2c,1,

            0,24,0x3b,1,

            128
        },

         //whipping climbing down stairs frame 2/3
        {
            0,0,0x43,0,     
            8,0,0x44,0,

            0,8,0x53,0,
            8,8,0x54,0,

            0,16,0x2b,1,
            8,16,0x2c,1,

            0,24,0x3b,1,

            128
        },

         //whipping climbing down stairs frame 3/3
        {
            8,0,0x46,0,
            16,0,0x47,0,

            0,8,0x55,0,
            8,8,0x56,0,
            16,8,0x57,0,

            0,16,0x2b,1,
            8,16,0x2c,1,

            0,24,0x3b,1,

            128
        },
    },


     //direction:right
    {
        //walking frame 1/4
        {
            8,0,0x00,0 | OAM_FLIP_H,     
            0,0,0x01,0 | OAM_FLIP_H,

            8,8,0x10,0 | OAM_FLIP_H,
            0,8,0x11,0 | OAM_FLIP_H,

            8,16,0x20,1 | OAM_FLIP_H,
            0,16,0x21,1 | OAM_FLIP_H,

            8,24,0x30,1 | OAM_FLIP_H,
            0,24,0x31,1 | OAM_FLIP_H,

            128
        },

        //walking frame 2/4
        {
            8,0,0x02,0 | OAM_FLIP_H,     
            0,0,0x03,0 | OAM_FLIP_H,

            8,8,0x12,0 | OAM_FLIP_H,
            0,8,0x13,0 | OAM_FLIP_H,

            8,16,0x22,1 | OAM_FLIP_H,
            0,16,0x23,1 | OAM_FLIP_H,

            8,24,0x32,1 | OAM_FLIP_H,
            0,24,0x33,1 | OAM_FLIP_H,

            128
        },

        //walking frame 3/4
        {
            8,0,0x04,0 | OAM_FLIP_H,     
            0,0,0x05,0 | OAM_FLIP_H,

            8,8,0x14,0 | OAM_FLIP_H,
            0,8,0x15,0 | OAM_FLIP_H,

            8,16,0x24,1 | OAM_FLIP_H,
            0,16,0x25,1 | OAM_FLIP_H,

            8,24,0x34,1 | OAM_FLIP_H,
            0,24,0x35,1 | OAM_FLIP_H,

            128
        },

        //walking frame 4/4 - same as walking 2/4



        //crouching frame 1/1
        {
            8,8,0x06,0 | OAM_FLIP_H,
            0,8,0x07,0 | OAM_FLIP_H,

            8,16,0x16,0 | OAM_FLIP_H,
            0,16,0x17,0 | OAM_FLIP_H,

            8,24,0x26,1 | OAM_FLIP_H,
            0,24,0x27,1 | OAM_FLIP_H,

            128
        },


        //walking up stairs frame 1/2
        {
            16,0,0x08,0 | OAM_FLIP_H,
            8,0,0x09,0 | OAM_FLIP_H,
            
            16,8,0x18,0 | OAM_FLIP_H,
            8,8,0x19,0 | OAM_FLIP_H,

            16,16,0x28,1 | OAM_FLIP_H,
            8,16,0x29,1 | OAM_FLIP_H,

            16,24,0x38,1 | OAM_FLIP_H,
            8,24,0x39,1 | OAM_FLIP_H,
            0,24,0x3a,1 | OAM_FLIP_H,

            128
        },

        //walking up stairs frame 2/2 - same as walking frame 2/4



        //walking down stairs frame 1/2
        {
            8,0,0x0b,0 | OAM_FLIP_H,
            0,0,0x0c,0 | OAM_FLIP_H,

            8,8,0x1b,0 | OAM_FLIP_H,
            0,8,0x1c,0 | OAM_FLIP_H,

            8,16,0x2b,1 | OAM_FLIP_H,
            0,16,0x2c,1 | OAM_FLIP_H,

            8,24,0x3b,1 | OAM_FLIP_H,

            128
        },

        //walking down stairs frame 2/2 - same as walking frame 2/4


        //dead frame 1/1
        {
            32,0,0x0d,0 | OAM_FLIP_H,

            32,8,0x1d,0 | OAM_FLIP_H,
            24,8,0x1e,0 | OAM_FLIP_H,
            16,8,0x2d,1 | OAM_FLIP_H,
            8,8,0x2e,1 | OAM_FLIP_H,
            0,8,0x2f,1 | OAM_FLIP_H,

            128
        },

        //whipping standing frame 1/3
        {
            16,0,0x40,0 | OAM_FLIP_H,
            8,0,0x41,0 | OAM_FLIP_H,
            0,0,0x42,0 | OAM_FLIP_H,

            16,8,0x50,0 | OAM_FLIP_H,
            8,8,0x51,0 | OAM_FLIP_H,
            0,8,0x52,0 | OAM_FLIP_H,

            16,16,0x60,1 | OAM_FLIP_H,
            8,16,0x61,1 | OAM_FLIP_H,
            0,16,0x62,1 | OAM_FLIP_H,

            16,24,0x70,1 | OAM_FLIP_H,
            8,24,0x71,1 | OAM_FLIP_H,

            128
        },

        //whipping standing frame 2/3
        {
            8,0,0x43,0 | OAM_FLIP_H,     
            0,0,0x44,0 | OAM_FLIP_H,

            8,8,0x53,0 | OAM_FLIP_H,
            0,8,0x54,0 | OAM_FLIP_H,

            8,16,0x63,1 | OAM_FLIP_H,
            0,16,0x64,1 | OAM_FLIP_H,

            8,24,0x73,1 | OAM_FLIP_H,
            0,24,0x74,1 | OAM_FLIP_H,

            128
        },


        //whipping standing frame 3/3
        {
            8,0,0x46,0 | OAM_FLIP_H,
            0,0,0x47,0 | OAM_FLIP_H,

            16,8,0x55,0 | OAM_FLIP_H,
            8,8,0x56,0 | OAM_FLIP_H,
            0,8,0x57,0 | OAM_FLIP_H,

            16,16,0x65,1 | OAM_FLIP_H,
            8,16,0x66,1 | OAM_FLIP_H,
            0,16,0x67,1 | OAM_FLIP_H,

            16,24,0x75,1 | OAM_FLIP_H,
            8,24,0x76,1 | OAM_FLIP_H,
            0,24,0x77,1 | OAM_FLIP_H,

            128
        },


        //whipping crouching frame 1/3
        {
            16,0,0x40,0 | OAM_FLIP_H,
            8,0,0x41,0 | OAM_FLIP_H,
            0,0,0x42,0 | OAM_FLIP_H,

            16,8,0x50,0 | OAM_FLIP_H,
            8,8,0x51,0 | OAM_FLIP_H,
            0,8,0x52,0 | OAM_FLIP_H,

            16,16,0x16,1 | OAM_FLIP_H,
            8,16,0x17,1 | OAM_FLIP_H,

            16,24,0x26,1 | OAM_FLIP_H,
            8,24,0x27,1 | OAM_FLIP_H,

            128
        },

         //whipping crouching frame 2/3
        {
            8,0,0x43,0 | OAM_FLIP_H,     
            0,0,0x44,0 | OAM_FLIP_H,

            8,8,0x53,0 | OAM_FLIP_H,
            0,8,0x54,0 | OAM_FLIP_H,

            8,16,0x16,1 | OAM_FLIP_H,
            0,16,0x17,1 | OAM_FLIP_H,

            8,24,0x26,1 | OAM_FLIP_H,
            0,24,0x27,1 | OAM_FLIP_H,

            128
        },

         //whipping crouching frame 3/3
        {
            8,0,0x46,0 | OAM_FLIP_H,
            0,0,0x47,0 | OAM_FLIP_H,

            16,8,0x55,0 | OAM_FLIP_H,
            8,8,0x56,0 | OAM_FLIP_H,
            0,8,0x57,0 | OAM_FLIP_H,

            16,16,0x16,1 | OAM_FLIP_H,
            8,16,0x17,1 | OAM_FLIP_H,

            16,24,0x26,1 | OAM_FLIP_H,
            8,24,0x27,1 | OAM_FLIP_H,

            128
        },



        //whipping climbing up stairs frame 1/3
        {
            16,0,0x40,0 | OAM_FLIP_H,
            8,0,0x41,0 | OAM_FLIP_H,
            0,0,0x42,0 | OAM_FLIP_H,

            16,8,0x50,0 | OAM_FLIP_H,
            8,8,0x51,0 | OAM_FLIP_H,
            0,8,0x52,0 | OAM_FLIP_H,

            16,16,0x28,1 | OAM_FLIP_H,
            8,16,0x29,1 | OAM_FLIP_H,

            16,24,0x38,1 | OAM_FLIP_H,
            8,24,0x39,1 | OAM_FLIP_H,
            0,24,0x3a,1 | OAM_FLIP_H,

            128
        },

         //whipping climbing up stairs frame 2/3
        {
            16,0,0x43,0 | OAM_FLIP_H,     
            8,0,0x44,0 | OAM_FLIP_H,

            16,8,0x53,0 | OAM_FLIP_H,
            8,8,0x54,0 | OAM_FLIP_H,

            16,16,0x28,1 | OAM_FLIP_H,
            8,16,0x29,1 | OAM_FLIP_H,

            16,24,0x38,1 | OAM_FLIP_H,
            8,24,0x39,1 | OAM_FLIP_H,
            0,24,0x3a,1 | OAM_FLIP_H,

            128
        },

         //whipping climbing up stairs frame 3/3
        {
            8,0,0x46,0 | OAM_FLIP_H,
            0,0,0x47,0 | OAM_FLIP_H,

            16,8,0x55,0 | OAM_FLIP_H,
            8,8,0x56,0 | OAM_FLIP_H,
            0,8,0x57,0 | OAM_FLIP_H,

            16,16,0x28,1 | OAM_FLIP_H,
            8,16,0x29,1 | OAM_FLIP_H,

            16,24,0x38,1 | OAM_FLIP_H,
            8,24,0x39,1 | OAM_FLIP_H,
            0,24,0x3a,1 | OAM_FLIP_H,

            128
        },


        //whipping climbing down stairs frame 1/3
        {
            16,0,0x40,0 | OAM_FLIP_H,
            8,0,0x41,0 | OAM_FLIP_H,
            0,0,0x42,0 | OAM_FLIP_H,

            16,8,0x50,0 | OAM_FLIP_H,
            8,8,0x51,0 | OAM_FLIP_H,
            0,8,0x52,0 | OAM_FLIP_H,

            16,16,0x2b,1 | OAM_FLIP_H,
            8,16,0x2c,1 | OAM_FLIP_H,

            16,24,0x3b,1 | OAM_FLIP_H,

            128
        },

         //whipping climbing down stairs frame 2/3
        {
            8,0,0x43,0 | OAM_FLIP_H,     
            0,0,0x44,0 | OAM_FLIP_H,

            8,8,0x53,0 | OAM_FLIP_H,
            0,8,0x54,0 | OAM_FLIP_H,

            8,16,0x2b,1 | OAM_FLIP_H,
            0,16,0x2c,1 | OAM_FLIP_H,

            8,24,0x3b,1 | OAM_FLIP_H,

            128
        },

         //whipping climbing down stairs frame 3/3
        {
            8,0,0x46,0 | OAM_FLIP_H,
            0,0,0x47,0 | OAM_FLIP_H,

            16,8,0x55,0 | OAM_FLIP_H,
            8,8,0x56,0 | OAM_FLIP_H,
            0,8,0x57,0 | OAM_FLIP_H,

            16,16,0x2b,1 | OAM_FLIP_H,
            8,16,0x2c,1 | OAM_FLIP_H,

            16,24,0x3b,1 | OAM_FLIP_H,

            128
        },
    },
};

const u8 metaspr_whips[2][2][3][25] =
{
    //NES whip 
    {
        //direction left
        {
            //frame 1/3
            {
                8,0,0x36,2,

                8,8,0x37,2,
                
                8,16,0x0a,2,

                0,24,0x1a,2,
                8,24,0x2a,2,

                128
            },

            //frame 2/3
            {
                0,0,0x0e,2,
                8,0,0x0f,2,

                8,8,0x13,2,
                16,8,0x3d,2,

                8,16,0x3e,2,
                16,16,0x3f,2,

                128
            },

            //frame 3/3
            {
                0,0,0x72,2,
                8,0,0x45,2,
                16,0,0x48,2,

                0,8,0x58,2,
                8,8,0x68,2,
                16,8,0x78,2,

                128
            }
        },

        //direction right
        {
            //frame 1/3
            {
                0,0,0x36,2 | OAM_FLIP_H,

                0,8,0x37,2 | OAM_FLIP_H,
                
                0,16,0x0a,2 | OAM_FLIP_H,

                8,24,0x1a,2 | OAM_FLIP_H,
                0,24,0x2a,2 | OAM_FLIP_H,

                128
            },

            //frame 2/3
            {
                16,0,0x0e,2 | OAM_FLIP_H,
                8,0,0x0f,2 | OAM_FLIP_H,

                8,8,0x13,2 | OAM_FLIP_H,
                0,8,0x3d,2 | OAM_FLIP_H,

                8,16,0x3e,2 | OAM_FLIP_H,
                0,16,0x3f,2 | OAM_FLIP_H,

                128
            },

            //frame 3/3
            {
                16,0,0x72,2 | OAM_FLIP_H,
                8,0,0x45,2 | OAM_FLIP_H,
                0,0,0x48,2 | OAM_FLIP_H,

                16,8,0x58,2 | OAM_FLIP_H,
                8,8,0x68,2 | OAM_FLIP_H,
                0,8,0x78,2 | OAM_FLIP_H,

                128
            }
        },
    },

    //SNES whip 
    {
        //direction left
        {
            //frame 1/3
            {
                8,0,0x49,2,

                8,8,0x59,2,
                
                8,16,0x69,2,

                0,24,0x79,2,
                8,24,0x7a,2,

                128
            },

            //frame 2/3
            {
                0,0,0x4a,2,
                8,0,0x4b,2,

                8,8,0x5a,2,
                16,8,0x5b,2,

                8,16,0x6a,2,
                16,16,0x6b,2,

                128
            },

            //frame 3/3
            {
                0,0,0x4c,2,
                8,0,0x4d,2,
                16,0,0x4e,2,

                0,8,0x5c,2,
                8,8,0x5d,2,
                16,8,0x5e,2,

                128
            }
        },

        //direction right
        {
            //frame 1/3
            {
                0,0,0x49,2 | OAM_FLIP_H,

                0,8,0x59,2 | OAM_FLIP_H,
                
                0,16,0x69,2 | OAM_FLIP_H,

                8,24,0x79,2 | OAM_FLIP_H,
                0,24,0x7a,2 | OAM_FLIP_H,

                128
            },

            //frame 2/3
            {
                16,0,0x4a,2 | OAM_FLIP_H,
                8,0,0x4b,2 | OAM_FLIP_H,

                8,8,0x5a,2 | OAM_FLIP_H,
                0,8,0x5b,2 | OAM_FLIP_H,

                8,16,0x6a,2 | OAM_FLIP_H,
                0,16,0x6b,2 | OAM_FLIP_H,

                128
            },

            //frame 3/3
            {
                16,0,0x4c,2 | OAM_FLIP_H,
                8,0,0x4d,2 | OAM_FLIP_H,
                0,0,0x4e,2 | OAM_FLIP_H,

                16,8,0x5c,2 | OAM_FLIP_H,
                8,8,0x5d,2 | OAM_FLIP_H,
                0,8,0x5e,2 | OAM_FLIP_H,

                128
            }
        },
    },
};