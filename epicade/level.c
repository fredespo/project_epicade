#include "main.h"
#include "level.h"


#pragma rodata-name (push,"PRG1")
#include "level1a_dat.h"
#include "level1a_attr.h"

#include "level1b_dat.h"
#include "level1b_attr.h"


#pragma rodata-name (pop)
#pragma rodata-name (push,"PRG2")

#include "level1c_dat.h"
#include "level1c_attr.h"

#include "level1d_dat.h"
#include "level1d_attr.h"

#include "level2a_dat.h"
#include "level2a_attr.h"

#pragma rodata-name (pop)
#pragma rodata-name (push,"PRG3")

#include "level2b_dat.h"
#include "level2b_attr.h"

#include "level2c_dat.h"
#include "level2c_attr.h"

#pragma rodata-name (pop)
#pragma rodata-name (push,"PRG4")

#include "level3a_dat.h"
#include "level3a_attr.h"

#include "level3b_dat.h"
#include "level3b_attr.h"

#include "level3c_dat.h"
#include "level3c_attr.h"


#pragma rodata-name (pop)
#pragma rodata-name (push,"PRG5")

#include "level3d_dat.h"
#include "level3d_attr.h"

#include "level4a_dat.h"
#include "level4a_attr.h"

#pragma rodata-name (pop)
#pragma rodata-name (push,"PRG6")

#include "level4b_dat.h"
#include "level4b_attr.h"

#include "level4c_dat.h"
#include "level4c_attr.h"

#include "level4d_dat.h"
#include "level4d_attr.h"

#pragma rodata-name (pop)
#pragma rodata-name (push,"PRG7")

#include "level5a_dat.h"
#include "level5a_attr.h"

#include "level5b_dat.h"
#include "level5b_attr.h"

#include "level5c_dat.h"
#include "level5c_attr.h"

#pragma rodata-name (pop)
#pragma rodata-name (push,"PRG8")

#include "level5d_dat.h"
#include "level5d_attr.h"

#include "level6a_dat.h"
#include "level6a_attr.h"

#pragma rodata-name (pop)
#pragma rodata-name (push,"PRG9")

#include "level6b_dat.h"
#include "level6b_attr.h"

#include "levelfinal_dat.h"
#include "levelfinal_attr.h"

#pragma rodata-name (pop)

