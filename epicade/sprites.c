#include "main.h"
#include "sprites.h"

#include "metasprites_player.h"	
#include "level.h"

//ADDED
//---------------------------------------------------------------------------------

extern u16 scr;

u16 player_x=24;
u8 player_y=80;
u8 player_tile;

u16 tileIndex;


void clearSprites()
{
	oam_clear();
	oam_off = 0;
}

void drawSpriteZero()
{
	oam_off = oam_spr(0, 55, 0x7f, 3, oam_off);
}

void drawSprite(struct sprite *spr)
{
	oam_off = oam_spr(spr->x, spr->y, spr->tileNum, spr->attr, oam_off);
}

void loadSpriteTileBG(struct sprite *spr)
{
	tileIndex = ((spr->y-64)/8) + (((scr/8)+3)*22);
}


u8 getBgTile(u16 index)
{
	return level1b[index];
}

u16 getBgIndex(u8 x, u8 y)
{
	return ((y-64)/8) + (((x + scr)/8)*22);
}



//---------------------------------------------------------------------------------