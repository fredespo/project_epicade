#ifndef _ENGINE_H
#define _ENGINE_H

// -----------------------------------
// Engine external vars



// -----------------------------------
// Engine internal vars

extern u16 level_pos_offs;
extern u8 nam_offs;


extern u8 *level_dat;
extern u8 *level_attr;
extern u8 *level_pal;
extern u16 level_len;
// -----------------------------------
extern void level_init(u8 num);
extern void level_render_col(void);

#endif // _ENGINE_H
