#ifndef _GAME_H
#define _GAME_H

extern void gameloop(void);
extern void gameinit(void);

extern u16 scr;

extern u16 level_pos_offs_r;
extern u16 level_pos_offs_l;
extern u8 nam_offs_l;
extern u8 nam_offs_r;


extern const u8 *level_dats[];
extern const u8 *level_attrs[];
extern const u8 *level_pals[];
extern const u16 level_lens[];
extern const u8 level_banks[];
extern const u8 level_chrs[];


#endif // _GAME_H
