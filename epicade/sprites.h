#ifndef _SPRITES_H
#define _SPRITES_H

extern u16 player_x;
extern u8 player_y;
extern u8 player_tile;

extern void clearSprites(void);
extern void drawSpriteZero(void);
extern void drawSprite(struct sprite *spr);
extern void loadSpriteTileBG(struct sprite *spr);
extern u8 getBgTile(u16 index);
extern u16 getBgIndex(u8 x, u8 y);

#endif
