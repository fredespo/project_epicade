#include "main.h"
#include "game.h"
#include "engine.h"
#include "level.h"

#pragma bss-name (push,"ZEROPAGE")
#pragma data-name (push,"ZEROPAGE")

u8 oam_off;

#pragma data-name(pop)
#pragma bss-name (pop)




void bankswitch(const u8 bank, const u8 file) {
	#define BANKSEL *((u8 *) 0x8000)
	#define BANKFILE *((u8 *) 0x8001)

	// Banks 0 and 1 are 2kb bg, 2,3,4,5 are 1kb sprite.
	// Banks 6 and 7 are switchable 8kb PRG banks.

	BANKSEL = (bank & 0x7);
	BANKFILE = file;

	#undef BANKSEL
	#undef BANKFILE
}

void str(const char *msg) {

	while (*msg) {
		vram_put(*msg - ' ');
		msg++;
	}
}


int main() {
	#define IRQDISABLE *((u8 *) 0xe000)
	IRQDISABLE = 1;

	// If the game used scrolling, you'd set the orientation here.
	// EDIT: right, and we will right here:

	#define MMC3MIRRORING *((u8 *) 0xA000)
	MMC3MIRRORING = 0;
	
	#define PPUCTRL *((u8 *) 0x2000)


	joy_install(joy_static_stddrv);

	// Load the font CHR into the first BG slot, and the first arrow
	// to the second BG slot. The arrow's number is 2, because the
	// font CHR is 2kb - two 1kb slots.

/*
	bankswitch(0, 0);
	bankswitch(1, 2);


	// Load level 1

	bankswitch(6,0);


	// Load sprite CHR data into bank 1
*/
    bankswitch(2,28);
    bankswitch(3,29);

    bank_spr(1);


// HACK: init level1
/*
	level_dat = (u8 *)level1a;
	level_attr = (u8 *)level1a_attr;
	level_pal = (u8 *)level1a_pal;
*/


	gameinit();
	gameloop();	

	return 0;
}
